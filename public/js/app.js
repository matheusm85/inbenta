var sessionToken;
var urlChatbot;
var accessToken;
var notFound;

function getHistory() {
    $.ajax({
        method: 'POST',
        url: '/yodabot/history',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            'sessionToken': sessionToken,
            'urlChatbot': urlChatbot,
            'accessToken': accessToken
        }
    }).done(function(data, statusText, xhr) {
        if (data.history.length > 0) {
            $('#output').append('<ol class="m-0">');
            data.history.forEach(function(msg, i) {
                if (msg['user'] == 'bot') {
                    $('#output').append('<li class="m-0">' + '<span class="fw-bold">YodaBot:</span> ' + msg['message'] + '</li>');
                } else {
                    $('#output').append('<li>' + '<span class="fw-bold">Me:</span> ' + msg['message'] + '</li>');
                }
            });
            $('#output').append('</ol>');

            $('#start').hide();
            $('#main').scrollTop($('#main')[0].scrollHeight);
            $('#message').val('');
            $('#message').focus();
        }
    });
}

$(document).on('click', '#start', function(e) {
    e.preventDefault();
    $(".starting").hide();
    $(this).parent().append('<p class="starting">Starting...</p>');
    $(this).hide();
    $("#logger").html('&nbsp;');
    $('#message').val('');
    $('#message').focus();
    $.ajax({
        method: 'GET',
        url: '/yodabot/startchat',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(data, statusText, xhr) {
        $(".starting").text('Chat started. Say hello!');
        sessionToken = data.sessionToken;
        urlChatbot = data.urlChatbot;
        accessToken = data.accessToken;
        localStorage.setItem('sessionToken', data.sessionToken);
        localStorage.setItem('urlChatbot', data.urlChatbot);
        localStorage.setItem('accessToken', data.accessToken);
        $('#main').scrollTop($('#main')[0].scrollHeight);
        $('#message').val('');
        $('#message').focus();
    }).fail(function(error) {
        $('#start').show();
        $(".starting").html('Request failed. Try again.');
    });
});

$(document).on('click', '#send', function(e) {
    e.preventDefault();
    
    $(".starting").hide();
    let message = $('#message').val().trim();

    if (sessionToken == null || urlChatbot == null || accessToken == null) {
        $('#message').val('');
        $('#message').focus();
        $("#logger").text('Need start the chat!');
        return false;
    }

    if (message == "") {
        $('#message').focus();
        $("#logger").text('Your message is blank!');
        return false;
    }

    $('#output').append('<li>' + '<span class="fw-bold">Me:</span> ' + message + '</li>');

    $('#message').val('');
    $('#message').focus();
    $("#logger").text('YodaBot is writing...');

    $.ajax({
        method: 'POST',
        url: '/yodabot/sendmessage',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            'message': message,
            'sessionToken': sessionToken,
            'urlChatbot': urlChatbot,
            'accessToken': accessToken,
            'notFound': notFound
        }
    }).done(function(data, statusText, xhr) {
        $("#logger").html('&nbsp;');
        $('#output').append('<li class="m-0">' + '<span class="fw-bold">YodaBot:</span> ' + data.answer + '</li>');

        if (data.list.length > 0) {
            $('#output').append('<ol class="m-0">');
            data.list.forEach(function(nome, i) {
                $('#output').append('<li class="m-0" style="padding-left: 30px; list-style-type: circle;">' + nome + '</li>');
            });
            $('#output').append('</ol>');
        }

        $('#main').scrollTop($('#main')[0].scrollHeight);
        $('#message').val('');
        $('#message').focus();
        notFound = data.notFound;
    }).fail(function(error) {
        if (typeof error.responseJSON.errors === 'undefined') {
            $("#logger").html('Request failed. Try again.');
        } else {
            $("#logger").html(error.responseJSON.errors.message[0]);
        }
    });
});

$(document).on('keydown', '#message', function(e) {
    if (e.keyCode === 13) {
        $("#send").click();
    }
});

$(document).ready(function() {
    sessionToken = localStorage.getItem('sessionToken');
    urlChatbot = localStorage.getItem('urlChatbot');
    accessToken = localStorage.getItem('accessToken');
    if (sessionToken !== null && urlChatbot !== null && accessToken !== null) {
        getHistory();
    }
});