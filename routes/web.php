<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\YodaBotController;

Route::get('/', [YodaBotController::class, 'index']);
Route::get('yodabot/startchat', [YodaBotController::class, 'startChat']);
Route::post('yodabot/sendmessage', [YodaBotController::class, 'sendMessage']);
Route::post('yodabot/history', [YodaBotController::class, 'getHistory']);
