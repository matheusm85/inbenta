<!doctype html>
<html lang="en" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <title>Inbenta Challenge</title>
        <style>
        .container {
            width: auto;
            max-width: 680px;
            padding: 0 15px;
        }
        #output {
            overflow-y: auto;
        }
        </style>
        <link rel="shortcut icon" type="image/x-icon" href="https://cdn0.iconfinder.com/data/icons/movies-8/64/yoda_hollywood_cinema_film-512.png"/>
        <link rel="icon" href="https://cdn0.iconfinder.com/data/icons/movies-8/64/yoda_hollywood_cinema_film-512.png"/>
        <link rel="apple-touch-icon" href="https://cdn0.iconfinder.com/data/icons/movies-8/64/yoda_hollywood_cinema_film-512.png">
    </head>
    <body class="d-flex flex-column h-100">
    
        <main class="overflow-auto mb-2" id="main">
            <div class="container">
                <h1>YodaBot</h1>
                <p class="lead">Chat History</p>
                <div class="row pt-2 pb-2 align-items-center">
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary" id="start">Start Chat</button>
                    </div>
                </div>
                <div id="output"></div>
            </div>
        </main>
        <footer class="footer mt-auto py-3 bg-light" id="footer">
            <div class="container pb-2">
                <span id="logger" class="fst-italic">&nbsp;</span>
                <div class="row align-items-center">
                    <div class="col-10">
                        <input type="text" id="message" class="form-control" autofocus>
                    </div>
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary" id="send">Send!</button>
                    </div>
                </div>
            </div>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
        <script src="{{ asset(mix('js/app.min.js')) }}"></script>
    </body>
</html>
