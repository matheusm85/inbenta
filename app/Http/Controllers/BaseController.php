<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Log;

class BaseController extends Controller
{
    /**
     * undocumented function summary
     *
     * @param  string  $method
     * @param  string  $endpoint
     * @param  string  $accessToken
     * @param  string|null  $inbentaSession
     * @param  array|null  $body
     * @return array
     **/
    public function chatBot(String $method, String $endpoint, String $accessToken, String $inbentaSession = null, array $body = [])
    {
        $header = [
            'Authorization' => 'Bearer ' . $accessToken,
            'x-inbenta-key' => env('INBENTA_KEY'),
            'Content-Type'  => 'application/json'
        ];

        if (!empty($inbentaSession)) {
            $header = array_merge($header, [
                'x-inbenta-session' => 'Bearer ' . $inbentaSession
            ]);
        }

        $client = new Client([
            'headers'     => $header,
            'form_params' => $body
        ]);
        
        $request = $client->request($method, $endpoint);

        return $this->getResult($request);
    }

    /**
     * Get access token and apis.
     *
     * @return array
     */
    public function getAccessToken()
    {
        $client = new Client([
            'base_uri' => env('INBENTA_URL_AUTH'),
            'headers'  => [
                'x-inbenta-key' => env('INBENTA_KEY'),
                'Content-Type'  => 'application/json'
            ],
            'form_params' => [
                'secret' => env('INBENTA_SECRET')
            ]
        ]);
        
        $request = $client->request('POST');

        return $this->getResult($request);
    }

    /**
     * Get request decoded.
     *
     * @param  ResponseInterface  $json
     * @return array
     */
    private function getResult(ResponseInterface $json)
    {
        return json_decode((string)$json->getBody(), true);
    }

    /**
     * Get films/people on graphql.
     *
     * @param  string  $entity
     * @return array
     */
    public function graphql(String $entity)
    {
        if ($entity == 'people') {
            $data = '{"query":"{allPeople(first: 10){people{name}}}"}';
        } elseif ($entity == 'films') {
            $data = '{"query":"{allFilms{films{title, episodeID}}}"}';
        }

        $client = new Client([
            'base_uri' => 'https://inbenta-graphql-swapi-staging.herokuapp.com/api',
            'headers'  => [
                'Content-Type'  => 'application/json'
            ],
            'body' => $data
        ]);

        $request = $client->request('POST');

        $filteredRequest = $this->getResult($request);

        $list = [];

        if ($entity == 'people') {
            foreach ($filteredRequest['data']['allPeople']['people'] as $item) {
                array_push($list, $item['name']);
            }
        } elseif ($entity == 'films') {
            foreach ($filteredRequest['data']['allFilms']['films'] as $item) {
                $list[(int) $item['episodeID'] - 1] = $item['title'];
            }
            ksort($list);
        }

        return $list;
    }
}
