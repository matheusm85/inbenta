const mix = require('laravel-mix');

mix.scripts(['public/js/app.js'], 'public/js/app.min.js');

if (mix.inProduction()) {
    mix.version();
}