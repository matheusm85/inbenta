<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class YodaBotController extends BaseController
{
    /**
     * Index /
     *
     * @return view
     **/
    public function index()
    {
        return view('YodaBot.index');
    }

    /**
     * Get new message
     *
     * @return array (sessionToken, urlChatbot, accessToken)
     **/
    public function startChat()
    {
        // Auth
        $authRequest = $this->getAccessToken();
        $accessToken = $authRequest['accessToken'];
        $urlChatbot  = $authRequest['apis']['chatbot'] . '/v1';

        // Start conversation
        $chatBot      = $this->chatBot('POST', $urlChatbot . '/conversation', $accessToken);
        $sessionToken = $chatBot['sessionToken'];
        
        return response()->json([
            'sessionToken' => $sessionToken,
            'urlChatbot'   => $urlChatbot,
            'accessToken'  => $accessToken
        ]);
    }

    /**
     * Get new message
     *
     * @param  array $request
     * @return array (answer, list, notFound)
     **/
    public function sendMessage(Request $request)
    {
        $list = [];
        $input = $request->all();

        $request->validate([
            'message'      => 'required',
            'urlChatbot'   => 'required',
            'accessToken'  => 'required',
            'sessionToken' => 'required'
        ]);

        $chatBot = $this->chatBot(
            'POST',
            $input['urlChatbot'] . '/conversation/message',
            $input['accessToken'],
            $input['sessionToken'],
            [
                'message' => $input['message']
            ]
        );

        $notFound = $input['notFound'] ?? 0;

        if ($this->isForce($input['message'])) {
            $answer = "The <b>force</b> is in this movies:";
            $list = $this->graphql('films');
        } else {
            if (isset($chatBot['answers'][0]['flags'][0])) {
                if ($chatBot['answers'][0]['flags'][0] == 'no-results') {
                    $notFound += 1;
                } else {
                    $notFound = 0;
                }
            } else {
                $notFound = 0;
            }

            if ($notFound >= 2) {
                $answer = "I haven't found any results, but here is a list of some Star Wars characters:";
                $list = $this->graphql('people');
                $notFound = 0;
            } else {
                $answer = "Try again...";
                if (isset($chatBot['answers'][0]['message'])) {
                    $answer = $chatBot['answers'][0]['message'];
                }
            }
        }

        return response()->json([
            'answer'   => $answer,
            'list'     => $list,
            'notFound' => $notFound
        ]);
    }

    /**
     * Get history
     *
     * @return array (sessionToken, urlChatbot, accessToken)
     **/
    public function getHistory(Request $request)
    {
        $input = $request->all();

        $request->validate([
            'urlChatbot'   => 'required',
            'accessToken'  => 'required',
            'sessionToken' => 'required'
        ]);

        $chatBot = $this->chatBot(
            'GET',
            $input['urlChatbot'] . '/conversation/history',
            $input['accessToken'],
            $input['sessionToken']
        );
        
        return response()->json([
            'sessionToken' => $input['sessionToken'],
            'urlChatbot'   => $input['urlChatbot'],
            'accessToken'  => $input['accessToken'],
            'history'      => $chatBot
        ]);
    }

    /**
     * Check if message have the word FORCE
     *
     * @param  string $message
     * @return boolean
     **/
    private function isForce(String $message)
    {
        $result = false;

        $explode = explode(' ', trim($message));

        $pos1 = isset($explode[0]) ? $explode[0] === 'force' : false;
        $pos2 = isset($explode[sizeof($explode) - 1]) ? $explode[sizeof($explode) - 1] === 'force' : false;
        $pos3 = strtolower($message) === 'force';
        $pos4 = stripos($message, ' force ');

        if ($pos1 !== false || $pos2 !== false || $pos3 !== false || $pos4 !== false) {
            $result = true;
        }

        return $result;
    }
}
